# Makefile pour les livres
# Copyright (C) 2020  Renard Rebelle

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

LIVRE_ID = identifiant-du-livre
LIVRE_VERSION = 1.3

SOURCE_FILE = ${LIVRE_ID}.livre

SOURCE_DIR = ./livre
RESOURCES_DIR = ./resources
BUILD_DIR = ./build
DIST_DIR = ./dist

COVER_FILENAME = couverture


.PHONY: all
all: pdf ebook


.PHONY: covers
covers: cover-png cover-pdf


.PHONY: cover-png
cover-png:
	@echo "Generating PNG cover..."
	@mkdir -p ${SOURCE_DIR}/images
	@inkscape --export-type=png --export-dpi=300 -o ${SOURCE_DIR}/images/${COVER_FILENAME}.png ${RESOURCES_DIR}/${COVER_FILENAME}.svg &> /dev/null
	@echo "Done."


.PHONY: cover-pdf
cover-pdf:
	@echo "Generating PDF cover..."
	@mkdir -p ${SOURCE_DIR}/images
	@inkscape --export-type=pdf --export-text-to-path -o ${SOURCE_DIR}/images/${COVER_FILENAME}.pdf ${RESOURCES_DIR}/${COVER_FILENAME}.svg &> /dev/null
	@echo "Done."


.PHONY: build-latex
build-latex: cover-pdf
	@mkdir -p ${BUILD_DIR}/latex
	@echo "Translating to LaTeX..."
	@livre -f latex -o ${BUILD_DIR}/latex/${LIVRE_ID}.tex ${SOURCE_DIR}/${SOURCE_FILE}
	@echo "Done."


.PHONY: latex
latex: build-latex
	@mkdir -p ${DIST_DIR}/latex
	@echo "Copying LaTeX file to '${DIST_DIR}/latex/'..."
	@cp ${BUILD_DIR}/latex/${LIVRE_ID}.tex ${DIST_DIR}/latex/
	@echo "Done."
	@echo "The LaTeX file is '${DIST_DIR}/latex/${LIVRE_ID}.tex'."


.PHONY: build-pdf
build-pdf: build-latex
	@mkdir -p ${BUILD_DIR}/pdf
	@echo "Generating PDF from LaTeX..."
	@for i in {1..2}; do \
		pdflatex \
			-jobname=${LIVRE_ID} \
			-output-directory=${BUILD_DIR}/pdf \
			${BUILD_DIR}/latex/${LIVRE_ID}.tex > /dev/null; \
	done
	@echo "Done."


.PHONY: pdf
pdf: build-pdf
	@mkdir -p ${DIST_DIR}/pdf
	@echo "Copying PDF file to '${DIST_DIR}/pdf/'..."
	@cp ${BUILD_DIR}/pdf/${LIVRE_ID}.pdf ${DIST_DIR}/pdf/
	@echo "Done."
	@echo "The PDF file is '${DIST_DIR}/latex/${LIVRE_ID}.pdf'."


.PHONY: ebook
ebook: epub3 mobi


.PHONY: build-epub3
build-epub3: cover-png
	@mkdir -p ${BUILD_DIR}/epub3
	@echo "Generating ePub3 ebook..."
	@livre -f epub3 -o ${BUILD_DIR}/epub3/${LIVRE_ID}.epub ${SOURCE_DIR}/${SOURCE_FILE}
	@echo "Done."


.PHONY: epub3
epub3: build-epub3
	@mkdir -p ${DIST_DIR}/epub3
	@echo "Copying ePub3 ebook to '${DIST_DIR}/epub3/'..."
	@cp ${BUILD_DIR}/epub3/${LIVRE_ID}.epub ${DIST_DIR}/epub3/
	@echo "Done."
	@echo "The ePub3 file is '${DIST_DIR}/epub3/${LIVRE_ID}.epub'."


.PHONY: build-mobi
build-mobi: build-epub3
	@mkdir -p ${BUILD_DIR}/mobi
	@echo "Generating MOBI ebook..."
	@ebook-convert \
		${BUILD_DIR}/epub3/${LIVRE_ID}.epub \
		${BUILD_DIR}/mobi/${LIVRE_ID}.mobi \
		--mobi-file-type=both \
		--toc-title="Sommaire" \
		> /dev/null
	@echo "Done."


.PHONY: mobi
mobi: build-mobi
	@mkdir -p ${DIST_DIR}/mobi
	@echo "Copying MOBI ebook to '${DIST_DIR}/mobi/'..."
	@cp ${BUILD_DIR}/mobi/${LIVRE_ID}.mobi ${DIST_DIR}/mobi/
	@echo "Done."
	@echo "The MOBI file is '${DIST_DIR}/mobi/${LIVRE_ID}.mobi'."


.PHONY: clean
clean:
	-rm -rf build dist


.PHONY: new
new:
	@echo "Creating base template..."
	@mkdir -p ${SOURCE_DIR} ${RESOURCES_DIR}
	@touch ${RESOURCES_DIR}/${COVER_FILENAME}.svg
	@printf "@livre-version: ${LIVRE_VERSION}\n@titre: \n@auteur: \n@date: \n@couverture-pdf: ./images/${COVER_FILENAME}.pdf\n@couverture-png: ./images/${COVER_FILENAME}.png" > ${SOURCE_DIR}/${SOURCE_FILE}
	@printf "${BUILD_DIR}\n${DIST_DIR}\n${SOURCE_DIR}/images" >> .gitignore
	@echo "Done. You can now initialize the git repository."
