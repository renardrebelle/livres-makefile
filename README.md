# Makefile pour les livres

Permet de générer différents formats de sortie à partir d'un fichier `livre` source.

## Créer un squelette

Copier le fichier `Makefile`, modifier la variable `LIVRE_ID` et lancer la commande `make new`.

## Dépendances

Pour générer les couvertures, le programme [Inkscape](https://inkscape.org/) ainsi que les polices [Anarchie](https://gitlab.com/renardrebelle/anarchie-font) et [CMU Concrete](https://cm-unicode.sourceforge.io/) sont nécessaires.

Pour générer les fichiers LaTeX et ePub3, le programme [`livre`](https://gitlab.com/renardrebelle/livre) est nécessaire.

Pour générer le fichier PDF à partir du LaTeX, une distribution LaTeX avec le programme `pdflatex` et les paquets suivants est nécessaire:

- `babel` (french)
- `fontenc`
- `fancyhdr`
- `geometry`
- `hyperref`
- `import`
- `inputenc`
- `lmodern`
- `pdfpages`
- `titlesec`
- `verse`

Pour générer le fichier MOBI à partir de l'ePub3, le programme `ebook-convert` fourni par [Calibre](https://calibre-ebook.com/) est nécessaire.

## Génération des fichiers redistribuables

Générer tous les formats:

```bash
make all
```

Générer uniquement le fichier PDF:

```bash
make pdf
```

Générer uniquement le fichier ePub3:

```bash
make epub3
```

Générer uniquement le fichier MOBI:

```bash
make mobi
```

Les fichiers générés se trouvent dans le dossier `./dist`.
